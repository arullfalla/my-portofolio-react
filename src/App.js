import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Router, Route, Link, Switch } from "react-router-dom";
import Navbar from './components/CustomNavbar';
import './App.css';
import Home from './components/Home';
import About from './components/About';
import Projects from './components/inputProjects';
import ShowProject from './components/showproject';
import Request from './components/Request';
import NoMatch from './components/NoMatch';
import Footer from './components/footer';

class App extends React.Component {
  render () {
  return (

      <BrowserRouter>
        <div>
        <Navbar>

        </Navbar>
        <main>
          <Switch>
          <Route path='/' exact component={Home} />
          <Route path='/about' exact component={About} />
          <Route path='/projects' exact component={Projects} />
          <Route path='/projects:id' component={ShowProject} />
          <Route path='/request' exact component={Request} />
          <Route component={NoMatch} />
          </Switch>
          <Switch>
            <Footer></Footer>
          </Switch>
        </main>
      </div>
      </BrowserRouter>
    
  );
}
}
export default App;



