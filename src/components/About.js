import React from 'react';
import {Col, CardImg, Row} from 'reactstrap';
import './About.css';
import ShowProject from './showproject';

class About extends React.Component {
    render () {
        return (
            <div>
                <CardImg src="https://i.ibb.co/dWWGX9r/adult-competition-concentration-929831-Cropped.jpg" className="headerabout" />
                <h3>What is?</h3>
                <Row>
                <Col xs={12} className="main-section"></Col>
                <Col xs={12} className="aboutcontent">

                <p><i>What is a Games Tester?</i><br/>
A games tester is someone who works for video game production companies to thoroughly test video games before the final version is released to the public. Also known as beta game testers, games testers receive a version of a game that is close to its final stages. They must then play the game a number of times, from start to finish, in order to uncover bugs or glitches within the game.

Without games testers, bugs and glitches would abound in games, possibly making them unplayable and destroying the reputation of the video game production company.

Games testers are needed for every platform and genre. Depending on the location and the company, testers will play games on XBox, Playstation, Nintendo Wii and PC platforms. Role playing games, massively multi-player online games, action games, and learning games are just some of the genres of games that a game tester must play and thoroughly evaluate prior to release.</p>

<p><i>What does a Games Tester do?</i><br/>
A games tester receives advanced copies of games in order to thoroughly test the game prior to public release. They must navigate all the menus to ensure that everything works correctly and takes the player to the correct sub-menu or stage in the game. If the game has more than one mode of play, such as beginner, intermediate or advanced, the tester must play through each of these modes from start to finish to expose any glitches. If the game offers play as more than one type of character, the tester must also play through the game with each character or avatar.

During gameplay, the tester attempts to discover hidden bugs by attempting every possible move or decision a player might make during normal gameplay. The tester may even attempt to do things within the game that an average player might not do, such as trying to get stuck in a wall or obstruction within the game. This type of bug is known as clipping. Once the character is stuck, the player is often unable to free the character, making further gameplay impossible. Testers note occurrences of these and similar game malfunctions.

After discovering a bug, game testers must write a description of what occurred along with directions that accurately describe how to find the bug in the game. After submitting this information to the programmers, testers may be asked to test revised editions of the game in order to create a stable version.</p>

<p><i>What is the workplace of a Games Tester like?</i><br/>
A games tester traditionally works from home, though some opportunities are available to work on-site at a video game production company. Testers have no set daily schedule, but once a game is received, they may often have to adhere to strict deadlines for various portions of the game. Game developers often set an expected release date for their games, so it is vital that beta testers return quality information about problems as soon as possible.

A games tester spends long hours in front of a television or computer screen and performs repetitive motions with their hands. While an office or space assigned specifically to the job of game testing is not required, it is important for gamers to work in a quiet and relaxing environment in order to devote their full attention to game play.</p>
                </Col>
                </Row>
            </div>
        )
    }
}

export default About;