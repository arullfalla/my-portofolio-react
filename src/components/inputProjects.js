import './inputProjects.css';
import Sliderproject from './slider';
import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import {Form, FormGroup, Label, Input, Button, FormText} from 'reactstrap'
import Project from './functionpro';
import ShowProject from './showproject'

export default class TambahProject extends React.Component {
    constructor(props){
        super(props)
        this.state ={
            title:'',
            imageUrl:'',
            description:'',
            redirect: false
        }
        this.AddNewProject = this.AddNewProject.bind(this)
        this.onChangeTitle = this.onChangeTitle.bind(this)
        this.onChangeUrl = this.onChangeUrl.bind(this)
        this.onChangeDesc = this.onChangeDesc.bind(this)
    }
    onChangeDesc(event){
        this.setState({
            description:event.target.value
        })
    }
    onChangeTitle(event){
        this.setState({
            title:event.target.value
        })
    }
    onChangeUrl(event){
        this.setState({
            imageUrl:event.target.value
        })
    }
    AddNewProject = () => {
        console.log(this.state)

        fetch(	"http://reduxblog.herokuapp.com/api/posts?key=arullfalla2",
        {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify({
            title: this.state.title,
            categories: this.state.imageUrl,
            content: this.state.description
        }),
        
    })
    

    .then(response => response.json());

    // .then(()=>{
    //     this.setState({
    //         redirect:true
    //     })
    // })

    }
    render() {
            if(this.state.redirect){
                return(
                <Redirect to="/projects"></Redirect>
                )
            }
        return (
            <div>
                <Form>
        <FormGroup>
          <Label for="projectname" placeholder="Input Your Project Title" >Project Name :</Label>
          <Input onChange={this.onChangeTitle} name="namepro" />
        </FormGroup>
        <FormGroup>
          <Label for="projectimage" placeholder="Input Your Project Image URL">Project Image URL :</Label>
          <Input onChange={this.onChangeUrl} name="image"  />
        </FormGroup>
        <FormGroup>
          <Label for="projectdesription" placeholder="Input Your Project Description">Project Description :</Label>
          <Input onChange={this.onChangeDesc} type="textarea" name="description" />
        </FormGroup>
        <Button onClick={this.AddNewProject}>Submit</Button>
      </Form>
      
      <Project></Project>
      <Sliderproject></Sliderproject>
            </div>

        )
    }
}