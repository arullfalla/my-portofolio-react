import React from 'react';
import {Link} from 'react-router-dom'
import {Jumbotron, Grid, Row, Col, CardImg, Button} from 'reactstrap'
import './Home.css';


class Home extends React.Component {
    render () {
        return (
            <div>
                <Jumbotron>
                    <h2>Welcome To My Web Portofolio</h2>
                    <p>This Web Contains All of My Recent Work</p>
                <Button href='/about'> Learn More </Button>
                </Jumbotron>
                <Row className="show-grid text-center">
                    <Col xs={2} className="personwrap">
                    <CardImg src="https://i.ibb.co/YcHfF9R/ava.jpg" circle className="headerimage" />
                        <h3>Fajrul Falach</h3>
                        </Col>
                        <Col xs={12} sm={10}>
                        <p><u>"Fajrul Falach"</u> known as experienced Quality Assurance with a demonstrated history of working in the computer games industry. Skilled in Negotiation, Customer Relationship Management (CRM), Customer Retention, Sales Management, and Game Testing. Strong Quality Assurance Professional.
                        <br/>
                        <br/>
                        My job description:
                        <br/>
ᴑ Testing, tuning and debugging a game and suggesting refinements that ensure its quality and playability. Testing involves playing a game over and over again, testing different levels and builds (incomplete ‘development versions’ of a game, sometimes with various features missing).<br/>

ᴑ Assuring quality in a game and finding all its flaws before it goes public.<br/>

ᴑ Responsible for checking that the game works, is easy to use, has actions that make sense, and contains fun gameplay.<br/>

ᴑ Responsible to write accurate and specific bug reports, and if possible providing descriptions of how the bug can be reproduced.<br/>

ᴑ Assigned to a single game during its entire production, or brought onto other projects as demanded by the department's schedule and specific needs.<br/>

ᴑ Work to deadlines and must understand production and marketing schedules.
See less<br/>
                        </p>
                        </Col>
                </Row>

            </div>
        )
    }
}

export default Home;