import React from 'react';
import { UncontrolledCarousel } from 'reactstrap';
import './slider.css';

const items = [
  {
    src: 'https://i.ibb.co/Kq6D7Nb/Webp-net-resizeimage-3.jpg',
    altText: '',
    caption: '',
    header: ''
  },
  {
    src: 'https://i.ibb.co/hspH6NW/Webp-net-resizeimage-1.jpg',
    altText: '',
    caption: '',
    header: ''
  },
  {
    src: 'https://i.ibb.co/5FmnXBX/Webp-net-resizeimage-2.jpg',
    altText: '',
    caption: '',
    header: ''
  }
];

const Sliderprojects = () => <UncontrolledCarousel items={items} />;

export default Sliderprojects;