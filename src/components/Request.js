import React from 'react';
import './Request.css';
import {Col, CardImg, Row} from 'reactstrap';

export default class Request extends React.Component {
    state = {
        firstName: "",
        lastName: "",
        username: "",
        email: "",
        titleproject: "",
        finishdays: "",
        budget:"",
//        redirect: false

    }

    change = e => {
        this.setState({
        [e.target.name]: e.target.value
        });
    };

    onSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
    }

    // AddNewProject(){
//        fetch(" ") jangan lupa component didmount
//        mdn fetch
//    }

    render() {
        return (
            <div className="formrequest">
        <Row>
          <Col xs="6">
          <CardImg src="https://i.ibb.co/Pz2Fvwr/apple-macbook-air-notepad-edited.jpg" className="headerabout"/>
            </Col>
          <Col xs="6">
          <form>
                First Name:
                <input
                name="firstName" className="firstName" 
                placeholder="First Name" value={this.state.firstName} onChange={e => this.change(e)}
                />
                <br />
                Last Name:<input 
                name="lastName" className="lastName"
                placeholder="Last Name" value={this.state.lastName} onChange={e => this.change(e)}
                />
                <br />
                Username:<input 
                name="username" className="username"
                placeholder="Username" value={this.state.username} onChange={e => this.change(e)}
                />
                <br />
                Email:<input 
                name="email" className="email"
                placeholder="example@email.com" type="email" value={this.state.email} onChange={e => this.change(e)}
                />
                <br />
                Tittle Project:<input
                name="titleproject" className="tittleproject"
                placeholder="Tittle Project" value={this.state.titleproject} onChange={e => this.change(e)}
                />
                <br />
                Finish Days:<input
                name="finishdays" className="finishdays"
                placeholder="Finish Days" value={this.state.finishdays} onChange={e => this.change(e)}
                />
                <br />
                Budget:<input 
                name="budget" className="budget"
                placeholder="Budget Offer" value={this.state.budget} onChange={e => this.change(e)}
                />
                <br />

                <button onClick={e => this.onSubmit(e)}>Submit</button>
            </form>
            </Col>
            </Row>
            
            </div>
        )
    }
  }