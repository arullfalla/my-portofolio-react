import React from 'react'
import { Link } from 'react-router-dom'


export default class Projects extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            projectArray:[]
        }
    }

    componentDidMount(){
        fetch('http://reduxblog.herokuapp.com/api/posts?key=arullfalla2')
        .then(response=>response.json())
        .then(data=>{
            this.setState(state=>({
                projectArray: [...state.projectArray, ...data]
            }))
        })
    }

    render() {
        //get()
        return (
            <div style={{ textAlign: 'center' }}>
                {/* <Link to='/projects'>
                    <button style={{marginBottom:'1em'}}>Add Project :</button>

                </Link> */}

                <ul style={{listStyle:'none', padding:0}}>
                    {
                        this.state.projectArray.map(project=>{
                            return(
                                <li style={{ padding:'1em'}}>
                                    <Link to={`/projects/${project.id}`}>
                                    <img src={project.categories} alt=""/>
                                    <p  style={{margin:0}}>{project.title}</p>
                                    </Link>
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
                )
            }
}